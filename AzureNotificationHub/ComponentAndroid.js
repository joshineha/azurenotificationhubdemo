import React, { Component } from 'react';
import { NativeEventEmitter } from 'react-native';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

const NotificationHub = require('react-native-azurenotificationhub');
const PushNotificationEmitter = new NativeEventEmitter(NotificationHub);

const EVENT_AZURE_NOTIFICATION_HUB_REGISTERED           = 'azureNotificationHubRegistered';
const EVENT_AZURE_NOTIFICATION_HUB_REGISTERED_ERROR     = 'azureNotificationHubRegisteredError';
const EVENT_REMOTE_NOTIFICATION_RECEIVED                = 'remoteNotificationReceived';

const connectionString = 'Endpoint=sb://pushhubnotificationtest.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=Qg1RqaNILHQ3+7zBBVqUGC7Hh8TlbrXnHO/xN5wwggs=';
const hubName = 'NewPushHub';
const tags =  ['testTagAndroid']; 
const senderID = '296399724728';               // The Sender ID from the Cloud Messaging tab of the Firebase console
const template = '{"data":{"message":"Notification Hub test notification"}}'//'{"data":{"message":"Notification Hub test notification"}}'  //'{"aps":{"alert":"Notification Hub test notification"}}';                                   
const templateName = 'Template Name';                // The set of tags to subscribe to.  See notes after code sample
const channelName = '...';            // The channel's name (optional)
const channelDescription = '...';     // The channel's description (optional)
const channelImportance = 3;          // The channel's importance (NotificationManager.IMPORTANCE_DEFAULT = 3) (optional)
                                      // Notes:
                                      //   1. Setting this value to 4 enables heads-up notification on Android 8
                                      //   2. On some devices such as Samsung Galaxy, changing this value requires
                                      //      uninstalling/re-installing the app to take effect.
const channelShowBadge = true;        // Optional
const channelEnableLights = true;     // Optional
const channelEnableVibration = true;  // Optional


export default class ComponentAndroid extends Component {
  constructor(props) {
    super(props);
    PushNotificationEmitter.addListener(EVENT_REMOTE_NOTIFICATION_RECEIVED, this._onRemoteNotification);
  }

  componentDidMount(){
    debugger
    console.log("ComponentAndroid")

  }
  register() {
    debugger
    PushNotificationEmitter.addListener(EVENT_AZURE_NOTIFICATION_HUB_REGISTERED, this._onAzureNotificationHubRegistered);
    PushNotificationEmitter.addListener(EVENT_AZURE_NOTIFICATION_HUB_REGISTERED_ERROR, this._onAzureNotificationHubRegistrationError);

    NotificationHub.register({
      connectionString,
      hubName,
      senderID,
      tags,
      channelName,
      channelDescription,
      channelImportance,
      channelShowBadge,
      channelEnableLights,
      channelEnableVibration
    })
    .then(console.log)
    .catch(console.warn);
  }

  registerTemplate() {
    debugger
    PushNotificationEmitter.addListener(EVENT_AZURE_NOTIFICATION_HUB_REGISTERED, this._onAzureNotificationHubRegistered);
    PushNotificationEmitter.addListener(EVENT_AZURE_NOTIFICATION_HUB_REGISTERED_ERROR, this._onAzureNotificationHubRegistrationError);

    NotificationHub.registerTemplate({
      connectionString,
      hubName,
      senderID,
      template,
      templateName,
      tags,
      channelName,
      channelDescription,
      channelImportance,
      channelShowBadge,
      channelEnableLights,
      channelEnableVibration
    })
    .then(console.log)
    .catch(console.warn);
  }

  getInitialNotification() {
    debugger
    NotificationHub.getInitialNotification()
    .then(console.log)
    .catch(console.warn);
  }

  getUUID() {
    debugger
    NotificationHub.getUUID(false)
    .then(console.log)
    .catch(console.warn);
  }

  isNotificationEnabledOnOSLevel() {
    NotificationHub.isNotificationEnabledOnOSLevel()
    .then(console.log)
    .catch(console.warn);
  }

  unregister() {
    NotificationHub.unregister()
    .then(console.log)
    .catch(console.warn);
  }

  unregisterTemplate() {
    NotificationHub.unregisterTemplate(templateName)
    .then(console.log)
    .catch(console.warn);
  }

  _onAzureNotificationHubRegistered(registrationID) {
    console.warn('RegistrationID: ' + registrationID);
  }

  _onAzureNotificationHubRegistrationError(error) {
    console.warn('Error: ' + error);
  }

  _onRemoteNotification(notification) {
    console.warn(notification);
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.register.bind(this)}>
         <View style={styles.button}>
           <Text style={styles.buttonText}>
             Register
           </Text>
         </View>
       </TouchableOpacity>
       <TouchableOpacity onPress={this.registerTemplate.bind(this)}>
         <View style={styles.button}>
           <Text style={styles.buttonText}>
             Register Template
           </Text>
         </View>
       </TouchableOpacity>
       <TouchableOpacity onPress={this.getInitialNotification.bind(this)}>
         <View style={styles.button}>
           <Text style={styles.buttonText}>
            Get initial notification
           </Text>
         </View>
       </TouchableOpacity>
       <TouchableOpacity onPress={this.getUUID.bind(this)}>
         <View style={styles.button}>
           <Text style={styles.buttonText}>
            Get UUID
           </Text>
         </View>
       </TouchableOpacity>
       <TouchableOpacity onPress={this.isNotificationEnabledOnOSLevel.bind(this)}>
         <View style={styles.button}>
           <Text style={styles.buttonText}>
            Check if notification is enabled
           </Text>
         </View>
       </TouchableOpacity>
       <TouchableOpacity onPress={this.unregister.bind(this)}>
         <View style={styles.button}>
           <Text style={styles.buttonText}>
             Unregister
           </Text>
         </View>
       </TouchableOpacity>
       <TouchableOpacity onPress={this.unregisterTemplate.bind(this)}>
         <View style={styles.button}>
           <Text style={styles.buttonText}>
             Unregister Template
           </Text>
         </View>
       </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#FFF',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#FFF',
		marginBottom: 5,
	},
	button: {
		backgroundColor: '#0071c9',
		borderRadius: 4,
		justifyContent: 'center',
		alignItems: 'center',
		padding: 14,
		marginVertical: 14,
	},
	buttonText: {
		color: '#FFF',
	},
});