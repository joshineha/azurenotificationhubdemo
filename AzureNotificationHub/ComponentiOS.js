import React, { Component } from 'react';
import {
  Alert,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

const NotificationHub = require('react-native-azurenotificationhub/index.ios');

const connectionString = 'Endpoint=sb://pushhubnotificationtest.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=Qg1RqaNILHQ3+7zBBVqUGC7Hh8TlbrXnHO/xN5wwggs=';
const hubName = 'NewPushHub';               
const tags =  ['testTag'];    
const template = '{"aps":{"alert":"Notification Hub test notification"}}';                                   
const templateName = 'Template Name';    
let remoteNotificationsDeviceToken = '';  

export default class ComponentiOS extends Component {

    componentDidMount(){
        debugger
      console.log("ComponentiOS")

    }
  requestPermissions() {
    //console.log("requestPermissions")
    NotificationHub.addEventListener('register', this._onRegistered);
    NotificationHub.addEventListener('registrationError', this._onRegistrationError);
    NotificationHub.addEventListener('registerAzureNotificationHub', this._onAzureNotificationHubRegistered);
    NotificationHub.addEventListener('azureNotificationHubRegistrationError', this._onAzureNotificationHubRegistrationError);
    NotificationHub.addEventListener('notification', this._onRemoteNotification);
    NotificationHub.addEventListener('localNotification', this._onLocalNotification);
    NotificationHub.requestPermissions()
      .then(console.log)
      .catch(console.warn);
  }

  register() {
    //debugger
    //console.log("register")
    NotificationHub.register(remoteNotificationsDeviceToken, { connectionString, hubName, tags })
      
    .then(console.log)
      .catch(console.warn);
  }

  registerTemplate() {
    //debugger
    //console.log("registerTemplate")
    NotificationHub.registerTemplate(remoteNotificationsDeviceToken, { connectionString, hubName, tags, templateName, template })
      .then(console.log)
      .catch(console.warn);
  }

  unregister() {
    NotificationHub.unregister()
      .then(console.log)
      .catch(console.warn);
  }

  unregisterTemplate() {
    NotificationHub.unregisterTemplate(templateName)
      .then(console.log)
      .catch(console.warn);
  }

  _onRegistered(deviceToken) {
    remoteNotificationsDeviceToken = deviceToken;
    Alert.alert(
      'Registered For Remote Push',
      `Device Token: ${deviceToken}`,
      [{
        text: 'Dismiss',
        onPress: () => {},
      }]
    );
  }

  _onRegistrationError(error) {
    Alert.alert(
      'Failed To Register For Remote Push',
      `Error (${error.code}): ${error.message}`,
      [{
        text: 'Dismiss',
        onPress: () => {},
      }]
    );
  }

  _onRemoteNotification(notification) {
    Alert.alert(
      'Push Notification Received',
      'Alert message: ' + notification.getMessage(),
      [{
        text: 'Dismiss',
        onPress: () => {},
      }]
    );
  }

  _onAzureNotificationHubRegistered(registrationInfo) {
    Alert.alert('Registered For Azure notification hub',
      'Registered For Azure notification hub',
      [{
        text: 'Dismiss',
        onPress: () => {},
      }]
    );
  }

  _onAzureNotificationHubRegistrationError(error) {
    Alert.alert(
      'Failed To Register For Azure Notification Hub',
      `Error (${error.code}): ${error.message}`,
      [{
        text: 'Dismiss',
        onPress: () => {},
      }]
    );
  }

  _onLocalNotification(notification){
    Alert.alert(
      'Local Notification Received',
      'Alert message: ' + notification.getMessage(),
      [{
        text: 'Dismiss',
        onPress: () => {},
      }]
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.requestPermissions.bind(this)}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>
              Request permission
            </Text> 
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.register.bind(this)}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>
              Register
            </Text> 
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.registerTemplate.bind(this)}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>
              Register Template
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.unregister.bind(this)}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>
              Unregister
            </Text> 
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.unregisterTemplate.bind(this)}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>
              Unregister Template
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

 
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#FFF',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#FFF',
		marginBottom: 5,
	},
	button: {
		backgroundColor: '#0071c9',
		borderRadius: 4,
		justifyContent: 'center',
		alignItems: 'center',
		padding: 14,
		marginVertical: 14,
	},
	buttonText: {
		color: '#FFF',
	},
});