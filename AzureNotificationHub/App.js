import {
  Platform
} from 'react-native';

export const App = Platform.select({
  ios: require('./ComponentiOS'),
  android: require('./ComponentAndroid'),
});








// import React, { Component } from 'react';
// import { NativeEventEmitter ,Platform} from 'react-native';
// import {
//   StyleSheet,
//   Text,
//   TouchableOpacity,
//   View,
// } from 'react-native';

// const NotificationHub = require('react-native-azurenotificationhub');
// const PushNotificationEmitter = new NativeEventEmitter(NotificationHub);

// const EVENT_AZURE_NOTIFICATION_HUB_REGISTERED           = 'azureNotificationHubRegistered';
// const EVENT_AZURE_NOTIFICATION_HUB_REGISTERED_ERROR     = 'azureNotificationHubRegisteredError';
// const EVENT_REMOTE_NOTIFICATION_RECEIVED                = 'remoteNotificationReceived';

// const connectionString = 'Endpoint=sb://pushhubnotificationtest.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=Qg1RqaNILHQ3+7zBBVqUGC7Hh8TlbrXnHO/xN5wwggs=';
// const hubName = 'NewPushHub';
// const tags =  ['testTagAndroid']; 
// const senderID = '296399724728';               // The Sender ID from the Cloud Messaging tab of the Firebase console
// const template = '{"data":{"message":"Notification Hub test notification"}}'//'{"data":{"message":"Notification Hub test notification"}}'  //'{"aps":{"alert":"Notification Hub test notification"}}';                                   
// const templateName = 'Template Name';                // The set of tags to subscribe to.  See notes after code sample
// const channelName = '...';            // The channel's name (optional)
// const channelDescription = '...';     // The channel's description (optional)
// const channelImportance = 3;          // The channel's importance (NotificationManager.IMPORTANCE_DEFAULT = 3) (optional)
//                                       // Notes:
//                                       //   1. Setting this value to 4 enables heads-up notification on Android 8
//                                       //   2. On some devices such as Samsung Galaxy, changing this value requires
//                                       //      uninstalling/re-installing the app to take effect.
// const channelShowBadge = true;        // Optional
// const channelEnableLights = true;     // Optional
// const channelEnableVibration = true;  // Optional


// export default class App extends Component {
//   constructor(props) {
//     super(props);
//     //PushNotificationEmitter.addListener(EVENT_REMOTE_NOTIFICATION_RECEIVED, this._onRemoteNotificationAndroid);
//   }

  // componentDidMount(){
  //   //debugger
  //   //console.log("ComponentAndroid")

  // }
  // androidRegister() {
  //   debugger
  //   PushNotificationEmitter.addListener(EVENT_AZURE_NOTIFICATION_HUB_REGISTERED, this._onAzureNotificationHubRegisteredAndroid);
  //   PushNotificationEmitter.addListener(EVENT_AZURE_NOTIFICATION_HUB_REGISTERED_ERROR, this._onAzureNotificationHubRegistrationErrorAndroid);

  //   NotificationHub.register({
  //     connectionString,
  //     hubName,
  //     senderID,
  //     tags,
  //     channelName,
  //     channelDescription,
  //     channelImportance,
  //     channelShowBadge,
  //     channelEnableLights,
  //     channelEnableVibration
  //   })
  //   .then(console.log)
  //   .catch(console.warn);
  // }

  // androidRegisterTemplate() {
  //   debugger
  //   PushNotificationEmitter.addListener(EVENT_AZURE_NOTIFICATION_HUB_REGISTERED, this._onAzureNotificationHubRegisteredAndroid);
  //   PushNotificationEmitter.addListener(EVENT_AZURE_NOTIFICATION_HUB_REGISTERED_ERROR, this._onAzureNotificationHubRegistrationErrorAndroid);

  //   NotificationHub.registerTemplate({
  //     connectionString,
  //     hubName,
  //     senderID,
  //     template,
  //     templateName,
  //     tags,
  //     channelName,
  //     channelDescription,
  //     channelImportance,
  //     channelShowBadge,
  //     channelEnableLights,
  //     channelEnableVibration
  //   })
  //   .then(console.log)
  //   .catch(console.warn);
  // }
 
  // _onAzureNotificationHubRegisteredAndroid(registrationID) {
  //   console.warn('RegistrationID: ' + registrationID);
  // }

  // _onAzureNotificationHubRegistrationErrorAndroid(error) {
  //   console.warn('Error: ' + error);
  // }

  // _onRemoteNotificationAndroid(notification) {
  //   console.warn(notification);
  // }

//   render() {
//     if (Platform.OS === ios) {
//       console.log('Running on ios!');
//       return (
//         <View style={styles.container}>
//           <TouchableOpacity 
//           //onPress={this.androidRegister.bind(this)}
//           >
//            <View style={styles.button}>
//              <Text style={styles.buttonText}>
//              iOS Register
//              </Text>
//            </View>
//          </TouchableOpacity>
//          <TouchableOpacity 
//          //onPress={this.androidRegisterTemplate.bind(this)}
//          >
//            <View style={styles.button}>
//              <Text style={styles.buttonText}>
//                iOS Register Template
//              </Text>
//            </View>
//          </TouchableOpacity>
//         </View>
//       );
//     }
//     else{
//       console.log('Running on android!');
//       return (
//         <View style={styles.container}>
//           <TouchableOpacity 
//           //onPress={this.androidRegister.bind(this)}
//           >
//            <View style={styles.button}>
//              <Text style={styles.buttonText}>
//                Android Register
//              </Text>
//            </View>
//          </TouchableOpacity>
//          <TouchableOpacity 
//          //onPress={this.androidRegisterTemplate.bind(this)}
//          >
//            <View style={styles.button}>
//              <Text style={styles.buttonText}>
//              Android Register Template
//              </Text>
//            </View>
//          </TouchableOpacity>
//         </View>
//       );
//     }
    
//   }
// }

// const styles = StyleSheet.create({
// 	container: {
// 		flex: 1,
// 		justifyContent: 'center',
// 		alignItems: 'center',
// 		backgroundColor: '#FFF',
// 	},
// 	welcome: {
// 		fontSize: 20,
// 		textAlign: 'center',
// 		margin: 10,
// 	},
// 	instructions: {
// 		textAlign: 'center',
// 		color: '#FFF',
// 		marginBottom: 5,
// 	},
// 	button: {
// 		backgroundColor: '#0071c9',
// 		borderRadius: 4,
// 		justifyContent: 'center',
// 		alignItems: 'center',
// 		padding: 14,
// 		marginVertical: 14,
// 	},
// 	buttonText: {
// 		color: '#FFF',
// 	},
// });


// import React, { Component } from 'react';
// import {
//   Alert,
//   StyleSheet,
//   Text,
//   TouchableOpacity,
//   View,
// } from 'react-native';

// const NotificationHub = require('react-native-azurenotificationhub/index.ios');

// const connectionString = 'Endpoint=sb://pushhubnotificationtest.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=Qg1RqaNILHQ3+7zBBVqUGC7Hh8TlbrXnHO/xN5wwggs=';
// const hubName = 'NewPushHub';               
// const tags =  ['testTag'];    
// const template = '{"aps":{"alert":"Notification Hub test notification"}}';                                   
// const templateName = 'Template Name';    
// let remoteNotificationsDeviceToken = '';  

// export default class App extends Component {

//     componentDidMount(){
//         debugger
//       console.log("ComponentiOS")

//     }
//   requestPermissions() {
//     //console.log("requestPermissions")
//     NotificationHub.addEventListener('register', this._onRegistered);
//     NotificationHub.addEventListener('registrationError', this._onRegistrationError);
//     NotificationHub.addEventListener('registerAzureNotificationHub', this._onAzureNotificationHubRegistered);
//     NotificationHub.addEventListener('azureNotificationHubRegistrationError', this._onAzureNotificationHubRegistrationError);
//     NotificationHub.addEventListener('notification', this._onRemoteNotification);
//     NotificationHub.addEventListener('localNotification', this._onLocalNotification);
//     NotificationHub.requestPermissions()
//       .then(console.log)
//       .catch(console.warn);
//   }

//   register() {
//     //debugger
//     //console.log("register")
//     NotificationHub.register(remoteNotificationsDeviceToken, { connectionString, hubName, tags })
      
//     .then(console.log)
//       .catch(console.warn);
//   }

//   registerTemplate() {
//     //debugger
//     //console.log("registerTemplate")
//     NotificationHub.registerTemplate(remoteNotificationsDeviceToken, { connectionString, hubName, tags, templateName, template })
//       .then(console.log)
//       .catch(console.warn);
//   }

//   unregister() {
//     NotificationHub.unregister()
//       .then(console.log)
//       .catch(console.warn);
//   }

//   unregisterTemplate() {
//     NotificationHub.unregisterTemplate(templateName)
//       .then(console.log)
//       .catch(console.warn);
//   }

//   _onRegistered(deviceToken) {
//     remoteNotificationsDeviceToken = deviceToken;
//     Alert.alert(
//       'Registered For Remote Push',
//       `Device Token: ${deviceToken}`,
//       [{
//         text: 'Dismiss',
//         onPress: () => {},
//       }]
//     );
//   }

//   _onRegistrationError(error) {
//     Alert.alert(
//       'Failed To Register For Remote Push',
//       `Error (${error.code}): ${error.message}`,
//       [{
//         text: 'Dismiss',
//         onPress: () => {},
//       }]
//     );
//   }

//   _onRemoteNotification(notification) {
//     Alert.alert(
//       'Push Notification Received',
//       'Alert message: ' + notification.getMessage(),
//       [{
//         text: 'Dismiss',
//         onPress: () => {},
//       }]
//     );
//   }

//   _onAzureNotificationHubRegistered(registrationInfo) {
//     Alert.alert('Registered For Azure notification hub',
//       'Registered For Azure notification hub',
//       [{
//         text: 'Dismiss',
//         onPress: () => {},
//       }]
//     );
//   }

//   _onAzureNotificationHubRegistrationError(error) {
//     Alert.alert(
//       'Failed To Register For Azure Notification Hub',
//       `Error (${error.code}): ${error.message}`,
//       [{
//         text: 'Dismiss',
//         onPress: () => {},
//       }]
//     );
//   }

//   _onLocalNotification(notification){
//     Alert.alert(
//       'Local Notification Received',
//       'Alert message: ' + notification.getMessage(),
//       [{
//         text: 'Dismiss',
//         onPress: () => {},
//       }]
//     );
//   }

//   render() {
//     return (
//       <View style={styles.container}>
//         <TouchableOpacity onPress={this.requestPermissions.bind(this)}>
//           <View style={styles.button}>
//             <Text style={styles.buttonText}>
//               Request permission
//             </Text> 
//           </View>
//         </TouchableOpacity>
//         <TouchableOpacity onPress={this.register.bind(this)}>
//           <View style={styles.button}>
//             <Text style={styles.buttonText}>
//               Register
//             </Text> 
//           </View>
//         </TouchableOpacity>
//         <TouchableOpacity onPress={this.registerTemplate.bind(this)}>
//           <View style={styles.button}>
//             <Text style={styles.buttonText}>
//               Register Template
//             </Text>
//           </View>
//         </TouchableOpacity>
//         <TouchableOpacity onPress={this.unregister.bind(this)}>
//           <View style={styles.button}>
//             <Text style={styles.buttonText}>
//               Unregister
//             </Text> 
//           </View>
//         </TouchableOpacity>
//         <TouchableOpacity onPress={this.unregisterTemplate.bind(this)}>
//           <View style={styles.button}>
//             <Text style={styles.buttonText}>
//               Unregister Template
//             </Text>
//           </View>
//         </TouchableOpacity>
//       </View>
//     );
//   }

 
// }

// const styles = StyleSheet.create({
// 	container: {
// 		flex: 1,
// 		justifyContent: 'center',
// 		alignItems: 'center',
// 		backgroundColor: '#FFF',
// 	},
// 	welcome: {
// 		fontSize: 20,
// 		textAlign: 'center',
// 		margin: 10,
// 	},
// 	instructions: {
// 		textAlign: 'center',
// 		color: '#FFF',
// 		marginBottom: 5,
// 	},
// 	button: {
// 		backgroundColor: '#0071c9',
// 		borderRadius: 4,
// 		justifyContent: 'center',
// 		alignItems: 'center',
// 		padding: 14,
// 		marginVertical: 14,
// 	},
// 	buttonText: {
// 		color: '#FFF',
// 	},
// });









// import {
//   Platform
// } from 'react-native';
// //import Android from './ComponentAndroid';
// //import Ios from './ComponentiOS';

// export const App = Platform.select({
//   ios: require('./ComponentiOS'),
//   android: require('./ComponentAndroid'),
// });
